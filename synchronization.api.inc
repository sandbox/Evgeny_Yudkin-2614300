<?php

/**
 * Implements synchronization_info().
 */
function syncronization_synchronization_info() {
  return array(
    'your_service_machine_name' => array(
      'entity_types' => array(
        'user',
      ),
      'timeout_period' => 100, // @todo
    ),
  );
}